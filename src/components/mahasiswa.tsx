import { FunctionComponent } from "react";

interface MahasiswaProps {
  urutan: number;
}

const MahasiswaComp: FunctionComponent<MahasiswaProps> = ({ urutan }) => {
  return (
    <>
      <div className="img-container">
        <img className="imgview" src={`https://i.pravatar.cc/150?img=${urutan}`} alt="" />
        Mahasiswa { urutan }
      </div>
    </>
  )
};

export default MahasiswaComp;
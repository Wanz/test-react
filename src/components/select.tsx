

interface ChildProps {
  updateValue: (position: number, value: number) => void,
  position: number
}

const SelectComp: React.FC<ChildProps> = (props) => {
  const rows = [];
  for (let i = 1; i < 11; i++) {
    rows.push(
      <option key={i} value={i}>{i}</option>
    )
  }
  
  const handleChange = (ev: React.ChangeEvent<HTMLSelectElement>) => {
    ev.preventDefault();
    // console.log(ev.target.value);
    props.updateValue(props.position, parseInt(ev.target.value))
  };
  
  return (
    <>
    <div className="select-container">
      <select onChange={handleChange} className="select-input" name="aspek">
        { rows }
      </select>
    </div>
    </>
  );
}

export default SelectComp;
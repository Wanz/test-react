import { useEffect, useState } from 'react'
import SelectComp from './components/select'
import MahasiswaComp from './components/mahasiswa'
import './App.css'

const TOTAL = 10;

function App() {
  const [count1, setCount1] = useState<number[]>([])
  const [count2, setCount2] = useState<number[]>([])
  const [count3, setCount3] = useState<number[]>([])
  const [count4, setCount4] = useState<number[]>([])
  const [textJson, setTextJson] = useState("")
  
  const headRows = [], bodyRows = [];
  for (let i = 0; i < 4; i++) {
    headRows.push(<td key={i}>Aspek<br /> penilaian { i + 1}</td>)
  }
  
  const addCount1 = (position: number, value: number) => {
    count1[position] = value;
    setCount1(count1);
  }
  
  const addCount2 = (position: number, value: number) => {
    count2[position] = value;
    setCount2(count2);
  }
  
  const addCount3 = (position: number, value: number) => {
    count3[position] = value;
    setCount3(count3);
  }
  
  const addCount4 = (position: number, value: number) => {
    count4[position] = value;
    setCount4(count4);
  }
  
  for (let i = 0; i < TOTAL; i++) {
    bodyRows.push(
      <tr key={i}>
        <td>{ <MahasiswaComp urutan={i+1} /> }</td>
        <td><SelectComp updateValue={addCount1} position={i} /></td>
        <td><SelectComp updateValue={addCount2} position={i} /></td>
        <td><SelectComp updateValue={addCount3} position={i} /></td>
        <td><SelectComp updateValue={addCount4} position={i} /></td>
      </tr>
    )
  }
  
  useEffect(()=>{
    const tmpCount1: number[] = [];
    const tmpCount2: number[] = [];
    const tmpCount3: number[] = [];
    const tmpCount4: number[] = [];
  
    for (let i = 0; i < TOTAL; i++) {
      tmpCount1.push(1);
      tmpCount2.push(1);
      tmpCount3.push(1);
      tmpCount4.push(1);
    }
    
    setCount1(tmpCount1);
    setCount2(tmpCount2);
    setCount3(tmpCount3);
    setCount4(tmpCount4);
	}, [])
  
  const handleSimpan = () => {    
    const result = {
      aspek_penilaian_1: {},
      aspek_penilaian_2: {},
      aspek_penilaian_3: {},
      aspek_penilaian_4: {},
    };
    for (let i = 0; i < TOTAL; i++) {
      Object.assign(result.aspek_penilaian_1, { ["mahasiswa_" + (i+1)]: count1[i] });
      Object.assign(result.aspek_penilaian_2, { ["mahasiswa_" + (i+1)]: count2[i] });
      Object.assign(result.aspek_penilaian_3, { ["mahasiswa_" + (i+1)]: count3[i] });
      Object.assign(result.aspek_penilaian_4, { ["mahasiswa_" + (i+1)]: count4[i] });
    }
    setTextJson(JSON.stringify(result, null, 2));
  };
  
  
  return (
    <>
      <div>
        <h1>Aplikasi Penilaian Mahasiswa</h1>
      </div>
      
      <div className='table-list'>
        <table className='table-mahasiswa'>
          <thead>
            <tr>
              <td></td>
              { headRows }
            </tr>
          </thead>
          <tbody>
            { bodyRows }
          </tbody>
        </table>
      </div>
      
      <div className='btn-container'>
        <button className='btn' onClick={handleSimpan}>Simpan</button>
      </div>
      
      { textJson != "" ? (
        <>
          <div className='output-container'>Output: </div>
          <div className='output'>
            <pre>
              { textJson }
            </pre>
          </div>
        </>
      ) : null }
    </>
  )
}

export default App
